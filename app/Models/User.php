<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='users';
    protected $fillable = [
        'name', 'email', 'password', 'id_role'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

        // Rest omitted for brevity

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    protected function rules()
    {
        return [
            'name'             => 'required|min:3|regex:/^[\pL\pM\p{Zs}.-]+$/u',
            'email'            => 'required|string|email|max:255|unique:users,email',
            'password'         => 'required|string',
            'confirm_password' => 'required|string',
            'id_role'          => 'required|integer',
        ];
    }

    protected function rulesUpdate()
    {
        return [
            'name'             => 'required|min:3|regex:/^[\pL\pM\p{Zs}.-]+$/u',
            'email'            => 'required|string|email|max:255',
            'id_role'          => 'required|integer|nullable',
        ];
    }

    protected function customMessages()
    {
        return [
            'name.required'                  => 'O campo nome é obrigatório',
            'name.regex'                     => 'O campo nome não aceita caracteres especiais',
            'id_role.required'               => 'O campo perfil é obrigatório',
            'id_role.integer'                => 'O campo perfil deve ser um número inteiro',
            'name.min'                       => 'O campo nome deve ter no mínimo 3 caracteres',
            'email.required'                 => 'O campo email é obrigatório',
            'email.email'                    => 'O campo email precisa conter um email válido',
            'email.unique'                   => 'Email já cadastrado',
            'email.max'                      => 'O campo email deve ter no máximo 255 caracteres',
            'password.required'              => 'O campo senha é obrigatório',
            'confirm_password.required'      => 'O campo confirmação de senha é obrigatório',
        ];
    }

    public function customValidate(Request $request)
    {
        $validator = Validator::make( $request->all(), $this->rules(), $this->customMessages());

        if ( $validator->fails() ) 
        {
            return $validator->errors();
        }
        else
        {
            return [];
        }
    }

    public function customValidateUpdate(Request $request)
    {
        $validator = Validator::make( $request->all(), $this->rulesUpdate(), $this->customMessages());

        if ( $validator->fails() ) 
        {
            return $validator->errors();
        }
        else
        {
            return [];
        }
    }
}