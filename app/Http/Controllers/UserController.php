<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::select('users.id as id', 'users.name as name', 'users.email as email', 'users.id_role as id_role', 'roles.description as role')
        ->join('roles', 'roles.id', '=', 'users.id_role')
        ->where(function($query){
            $query->when(auth()->user()->id_role != 1,function($query){
                $query->where('users.id', auth()->user()->id);                
            });
        })
        ->where(function($query){
            $query->when(request('pesquisa'),function($query){
                $query->where('name', 'like', '%'.request('pesquisa').'%')
                        ->orWhere('email', 'like', '%'.request('pesquisa').'%')
                        ->orWhere('role', 'like', '%'.request('pesquisa').'%');                
            });
        })
        ->orderBy(request('th') ? request('th') : 'users.name', request('order') ? request('order') : 'asc')
        ->paginate(request('qtd') ? request('qtd') : 10);

        return $user;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            if(auth()->user()->id_role != 1){
                return response()->json(['error' => "Você não ter permissão para cadastrar usuários"],404);
            }
            if(request('password') != request('confirm_password')){
                return response()->json(['error' => "As senhas não conferem"],404);
            }
            $userDefault = new User();
            $errorsUser = $userDefault->customValidate($request);
            if($errorsUser)
            {
                return response()->json(['error' => $errorsUser],404);
            }
            
            $user =  User::Create([
                'name'          => request('name'),
                'id_role'       => request('id_role'),
                'email'         => request('email'),
                'password'      => Hash::make(request('password'))
            ]);
            $user->save();
            DB::commit();
            return response()->json(['status' => true,'success'=>'Email '.request('email').' cadastrado com sucesso'],200);

        }catch (\Exception $e) {        
            DB::rollback();
            return response()->json(['error'=>'Não foi possível registrar o usuário. Contate o suporte.'.$e],404);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        if(auth()->user()->id_role != 1 && auth()->user()->id != $id){
            return response()->json(['error' => "Você não ter permissão para visualizar outros usuários"],404);
        }
        return User::findOrFail($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            if(auth()->user()->id_role != 1 && auth()->user()->id != $id){
                return response()->json(['error' => "Você não ter permissão para editar outros usuários"],404);
            }
            $userDefault = new User();
            $errorsUser = $userDefault->customValidateUpdate($request);
            if($errorsUser)
            {
                return response()->json(['error' => $errorsUser],404);
            }
            $user = User::findOrFail($id);
            if(auth()->user()->id_role != 1){
                $user->update([
                    'name'          => request('name'),
                    'email'         => request('email'),
                ]);
            }else{
                $user->update([
                    'name'          => request('name'),
                    'id_role'       => request('id_role'),
                    'email'         => request('email'),
                ]);
            }
            DB::commit();
            return response()->json(['status' => true,'success'=>'Usuário editado com sucesso'],200);

        }catch (\Exception $e) {        
            DB::rollback();
            return response()->json(['error'=>'Não foi possível editar o usuário. Contate o suporte.'.$e],404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::beginTransaction();
        try{
            if(auth()->user()->id_role != 1 && auth()->user()->id != $id){
                return response()->json(['error' => "Você não ter permissão para excluir outros usuários"],404);
            }
            $user = User::findOrFail($id);
            $user->delete();
            DB::commit();
            return response()->json(['status' => true,'success'=>'Usuário excluido com sucesso'],200);

        }catch (\Exception $e) {        
            DB::rollback();
            return response()->json(['error'=>'Não foi possível excluir o usuário. Contate o suporte.'.$e],404);
        }
    }

    public function updatePassword(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            if(auth()->user()->id_role != 1 && auth()->user()->id != $id){
                return response()->json(['error' => "Você não ter permissão para editar senha de outros usuários"],404);
            }
            $user = User::findOrFail(auth()->user()->id);
            $userClone = clone $user;
        
            $credentials = ["email" => $user->email, "password" => request('password')];
            if (! $token = auth()->attempt($credentials)){
                return response()->json(['errorNewPassword' => 'Senha atual inválida'], 401);
            }
            
            if( request('new_password') != request('confirm_password') ){
                return response()->json(['error' => 'As senhas não conferem'],401);  
            }
            if(request('new_password') == "")
            {
                return response()->json(['errorNewPassword' => 'Senha não inserida']);
            }
            
            $user->update([
                'password'=>Hash::make(request('new_password')),
            ]);
            
            DB::commit();
            return response()->json(['success' => 'Senha alterada com sucesso']);

        }catch(\Exception $e){
            return response()->json(['error' => 'Erro ao atualizar sua senha. Tente novamente mais tarde !'],401);
        }

    }
}
